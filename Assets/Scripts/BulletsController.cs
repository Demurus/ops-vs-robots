﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsController : MonoBehaviour
{
    protected Camera mainCamera;
    public GameObject ConcreteBulletHole;
    public GameObject MetalBulletHole;
    public GameObject WoodBulletHole;
    public GameObject EnemyBulletHit;

    [SerializeField] protected AudioClip[] _concreteImpacts;
    [SerializeField] protected AudioClip[] _woodImpacts;
    [SerializeField] protected AudioClip[] _metalImpacts;

    protected AudioSource _audioSource;
    //public delegate void OnEnemyHitDelegate(int damage); //doesn't work, but should. Do not delete
    //public event OnEnemyHitDelegate EnemyIsHit; //doesn't work, but should. Do not delete

    private void Awake()
    {
        mainCamera = Camera.main;
        _audioSource = GetComponent<AudioSource>();
    }

    public void CollisionDetector(string objectHitTag, Vector3 hitPosition, Vector3 hitNormal, GameObject hit, int damage)
    {
        //я помню, что инстанцировать - плохо, но времени допилить это в пул менеджер не хватило(
        if (objectHitTag == EnvironmentTags.Concrete)
        {
            Instantiate(ConcreteBulletHole, hitPosition, Quaternion.LookRotation(hitNormal));
            _audioSource.PlayOneShot(_concreteImpacts[UnityEngine.Random.Range(0, _concreteImpacts.Length)]);
        }

        if (objectHitTag == EnvironmentTags.Wood)
        {
            Instantiate(WoodBulletHole, hitPosition, Quaternion.LookRotation(hitNormal));
            _audioSource.PlayOneShot(_woodImpacts[UnityEngine.Random.Range(0, _woodImpacts.Length)]);
        }

        if (objectHitTag == EnvironmentTags.Metal)
        {
            Instantiate(MetalBulletHole, hitPosition, Quaternion.LookRotation(hitNormal));
            _audioSource.PlayOneShot(_metalImpacts[UnityEngine.Random.Range(0, _metalImpacts.Length)]);
        }

        if (objectHitTag == CommonTags.Enemytag)
        {

            Instantiate(EnemyBulletHit, hitPosition, Quaternion.LookRotation(hitNormal));
            _audioSource.PlayOneShot(_metalImpacts[UnityEngine.Random.Range(0, _metalImpacts.Length)]);
            hit.SendMessage(SendMessagesTags.ApplyDamage, damage);
            //EnemyIsHit(damage); doesn't work, but should. Do not delete



        }
        
    }


  
}
