﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public enum EnemyState
{
    Patrolling,
    ChasingShooting,
    StandingShooting,
    Dead
}
public class CommonEnemy : MonoBehaviour
{
    private EnemyAnimator EnemyAnimator;
    protected NavMeshAgent _agent;
    protected PlayerScript _player;
    public Camera EnemyCamera;
  
    public GameObject MuzzleFlash;
   
    public BulletsController _bulletsController;
  
    [SerializeField] private AudioSource __audioSource;

    private bool _isDead = false;

    public float ShootingInterval = 2f;

    private float _shootingTimer;
    private EnemyState enemyState;
    
    public float ShootingDistance = 10f;
    public float ShootingStandDistance = 3f;
    public float chaseAfterStandShootDistance = 2f;

    protected int health = 100;
    protected int damage = 50;

    public float WalkSpeed=1.5f;
    public float RunSpeed=4.0f;

    public float _chasingDistance = 15f;
    private float currentChasingDistance;
   
    public float _chasingTimer;
    public float _chasingInterval = 2f;

    public float patrolRadiusMinimum = 20f;
    public float patrolRadiusMaximum = 60f;
    public float patrolBeforeGivenNextDestination = 15f;
    private float patrolTimer;

    public float waitBeforeStandShoot = 2f;
    
   


    private void Awake()
    {
        _player = GameObject.FindGameObjectWithTag(CommonTags.PlayerTag).GetComponent<PlayerScript>();
        
        _agent = GetComponent<NavMeshAgent>();
        EnemyAnimator = GetComponent<EnemyAnimator>();
        _bulletsController = FindObjectOfType<BulletsController>();
       
    
    }

    //void EnemyHitHadler(int damage) //doesn't work, but should. Do not delete
    //{
    //    health -= damage;
    //    Debug.Log(health);
    //}

        public void ApplyDamage(int damage)
    {
        health -= damage;
            Debug.Log(health);
        if (health <= 0)
        {
            enemyState = EnemyState.Dead;
            MuzzleFlash.SetActive(false);
            __audioSource.Stop();
            _agent.isStopped = true;
            _agent.velocity = Vector3.zero;
            
            _agent.enabled = false;
           
            EnemyAnimator.WalkAnimation(false);
            EnemyAnimator.RunAnimation(false);

            EnemyAnimator.Dead();

        }
    }

    private void Start()
    {
        enemyState = EnemyState.Patrolling;
        patrolTimer = patrolBeforeGivenNextDestination;
       
        currentChasingDistance = _chasingDistance;
        _shootingTimer = Random.Range(0, ShootingInterval);
        _agent.SetDestination(_player.transform.position);
        //_bulletsController.EnemyIsHit += new BulletsController.OnEnemyHitDelegate(EnemyHitHadler); //doesn't work, but should. Do not delete

    }

    private void Update()
    {
        if (_isDead)
        {
            return;
        }
       
        if (enemyState == EnemyState.Patrolling) Patrolling();
        if (enemyState == EnemyState.ChasingShooting)
        {
            Chasing();   
        }
        if (enemyState == EnemyState.StandingShooting)
        {
            StandShoot();
           
        }

    }
    
    

    public void SetNewRandomDestination()
    {
        float randomRadius = Random.Range(patrolRadiusMinimum, patrolRadiusMaximum);
        Vector3 randomDirection = Random.insideUnitSphere * randomRadius;
        randomDirection += this.transform.position;
        NavMeshHit navHit;
        NavMesh.SamplePosition(randomDirection, out navHit, randomRadius, -1);
        _agent.SetDestination(navHit.position);
    }
    public void Patrolling()
    {
        MuzzleFlash.SetActive(false);
        _agent.isStopped = false;
        _agent.speed = WalkSpeed;
        
        patrolTimer += Time.deltaTime;
        if (patrolTimer > patrolBeforeGivenNextDestination)
        {
            SetNewRandomDestination();
            patrolTimer = 0f;
        }
        if (_agent.velocity.magnitude > 0)
        {
            EnemyAnimator.WalkAnimation(true);
        }
        else
        {
            EnemyAnimator.WalkAnimation(false);
        }
        if (Vector3.Distance(transform.position, _player.transform.position) <= _chasingDistance)
        {
            EnemyAnimator.WalkAnimation(false);
            enemyState = EnemyState.ChasingShooting;
        }
    }
    public void Shooting(float incomingShootingInterval)
    {
        
        _shootingTimer -= Time.deltaTime;
        if (_shootingTimer <= 0 && Vector3.Distance(transform.position, _player.transform.position) <= ShootingDistance)
        {
            
            _shootingTimer = incomingShootingInterval;
            
            RaycastHit hit;
            if (Physics.Raycast(EnemyCamera.transform.position, EnemyCamera.transform.forward, out hit))
            {
                
                if (hit.transform.gameObject.tag == CommonTags.PlayerTag)
                {
                    hit.transform.gameObject.SendMessage(SendMessagesTags.ApplyDamageToPlayer, 3);
                }
            }
        }
    }
    
    public void Chasing()
    {
        MuzzleFlash.SetActive(false);
        _agent.isStopped = false;
        _agent.speed = RunSpeed;
        _chasingTimer -= Time.deltaTime;
        if (_chasingTimer <= 0 && Vector3.Distance(transform.position, _player.transform.position) < _chasingDistance)
        {
            _chasingTimer = _chasingInterval;
        _agent.SetDestination(_player.transform.position);
            Shooting(2);
        }
        if (_agent.velocity.magnitude > 0)
        {
            EnemyAnimator.RunAnimation(true);
        }
        else
        {
            EnemyAnimator.RunAnimation(false);
        }
        if (Vector3.Distance(transform.position, _player.transform.position) <= ShootingStandDistance)
        {
           
            __audioSource.Play();
            EnemyAnimator.RunAnimation(false);
            EnemyAnimator.WalkAnimation(false);
            enemyState = EnemyState.StandingShooting;
            if (_chasingDistance != currentChasingDistance)
            {
                _chasingDistance = currentChasingDistance;
            }
            
        } else if (Vector3.Distance(transform.position, _player.transform.position) > _chasingDistance)
          {
              EnemyAnimator.RunAnimation(false);
              enemyState = EnemyState.Patrolling;
              patrolTimer = patrolBeforeGivenNextDestination;
           
              if (_chasingDistance != currentChasingDistance)
              {
                _chasingDistance = currentChasingDistance;
              }
          }
    }

    public void StandShoot()
    {
        _agent.velocity = Vector3.zero;
        _agent.isStopped = true;
        Vector3 direction = (_player.transform.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 9.0f);
        _agent.SetDestination(_player.transform.position);
        EnemyAnimator.ShootAnimation(true);
        Shooting(0.1F);
        MuzzleFlash.SetActive(true);
        
        
        
        if (Vector3.Distance(transform.position, _player.transform.position)>chaseAfterStandShootDistance)
        {
            
            __audioSource.Stop();
            enemyState = EnemyState.ChasingShooting;
        }
    }

}
