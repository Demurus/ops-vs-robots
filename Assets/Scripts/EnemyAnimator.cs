﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimator : MonoBehaviour
{
    [SerializeField] private Animator _animator;
   
    public void WalkAnimation(bool walk)
    {
        _animator.SetBool(EnemyAnimationTags.WalkBool, walk);
    }
    public void RunAnimation(bool run)
    {
        _animator.SetBool(EnemyAnimationTags.RunBool, run);
    }
    public void ShootAnimation(bool attack)
    {
        _animator.SetBool(EnemyAnimationTags.ShootTrigger, attack);
    }
        public void Dead()
    {
        _animator.SetTrigger(EnemyAnimationTags.DeadTrigger);
    }

}
