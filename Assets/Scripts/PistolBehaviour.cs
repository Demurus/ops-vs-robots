﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolBehaviour : WeaponsModel
{

    
    protected float FireTimer = 0.5f;
    protected int ClipSize = 17;
    protected int CurrentClipAmmo = 17;
    private float ReloadTime = 1.333f;
   
    private WeaponsModel _weaponsModel;
  
    private Animator _animator;
  
    
    [SerializeField] private AudioClip[] _ShotSounds;
    [SerializeField] private AudioClip _reloadSound;
    [SerializeField] private AudioClip _emptyClipSound;

    public int CurrentClip
    {
        get => CurrentClipAmmo;

        set
        {
            CurrentClipAmmo = Mathf.Clamp(value, 0, ClipSize);
        }
    }
    private void Awake()
    {
      
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    public void TurnOnMuzzleFlash()
    {
            GameObject gunMuzzleFlash = GunMuzzleFlashPoolingManager.Instance.GetGunMuzzleFlashPrefab();
            StartCoroutine(DisableGunMuzzleFlashTimer(gunMuzzleFlash));
    }
    public override void Shoot()
    {
       
        if (canShoot != true)
        {
            return;
        }
        else
        {
            CurrentClip--;
           
            if (CurrentClip == 0)
            {
                PlayEmptyClipSound(_emptyClipSound);
                return;
            }
            else
            {
                PlayShotSound(_ShotSounds[Random.Range(0, _ShotSounds.Length)]);
                GameObject simpleBullet = PoolingManager.Instance.GetSimpleBullet();
                ShootAnimation();
                canShoot = false;
                StartCoroutine(TimeToNexFire(FireTimer));
                StartCoroutine(DisableBulletTimer(simpleBullet));
            }

        }

    }
    

    

    public override void Reload()
    {
        canShoot = false;
        ReloadAnimation();
        PlayReloadSound(_reloadSound);
        StartCoroutine(ReloadTimer(ReloadTime));
        CurrentClipAmmo = ClipSize;

    }
   
}
