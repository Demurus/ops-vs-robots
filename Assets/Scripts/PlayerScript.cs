﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private AudioClip[] _bulletImpacts;

    private AudioSource _audioSource;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    public void ApplyDamageToPlayer(int damage)
    {
        _audioSource.PlayOneShot(_bulletImpacts[Random.Range(0, _bulletImpacts.Length)]);
        _health -= damage;
        Debug.Log(_health);
    }

    
}
