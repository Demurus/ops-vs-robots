﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleBehaviour : WeaponsModel
{ 
    protected float FireTimer = 0.2f;
    protected float ReloadTime = 2.2f;
    protected int ClipSize = 30;
    protected int CurrentClipAmmo = 30;

    [SerializeField] private AudioClip[] _ShotSounds;
    [SerializeField] private AudioClip _reloadSound;
    [SerializeField] private AudioClip _emptyClipSound;

   

    public int CurrentClip
    {
        get => CurrentClipAmmo;

        set
        {
            CurrentClipAmmo = Mathf.Clamp(value, 0, ClipSize);
        }
    }
    private void Awake()
    {
       
        _audioSource = GetComponent<AudioSource>();

    }
    public override void Shoot()
    {

        if (canShoot != true)
        {
            return;

        }
        CurrentClip--;
        Debug.Log(CurrentClip);
        if (CurrentClip == 0)
        {
            PlayEmptyClipSound(_emptyClipSound);
            return;
        }
        else
        {
           
            GameObject simpleBullet = PoolingManager.Instance.GetSimpleBullet();
            ShootAnimation();
            PlayShotSound(_ShotSounds[Random.Range(0, _ShotSounds.Length)]);
            canShoot = false;
            StartCoroutine(TimeToNexFire(FireTimer));
            StartCoroutine(DisableBulletTimer(simpleBullet));
        }

    }
   

    public void TurnOnMuzzleFlash()
    {
        GameObject rifleMuzzleFlash = RifleMuzzleFlashPoolingManager.Instance.GetRifleMuzzleFlashPrefab();
        StartCoroutine(DisableGunMuzzleFlashTimer(rifleMuzzleFlash));
    }

    public override void Reload()
    {
        canShoot = false;
        ReloadAnimation();
        PlayReloadSound(_reloadSound);
        StartCoroutine(ReloadTimer(ReloadTime));
        CurrentClipAmmo = ClipSize;

    }

   
   
}
