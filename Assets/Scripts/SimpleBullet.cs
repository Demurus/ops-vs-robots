﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : BulletsController
{
    private int _damage = 5;

   
    public void Fire()
    {
        RaycastHit hit;
        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit))
        {
            CollisionDetector(hit.transform.gameObject.tag, hit.point, hit.normal, hit.transform.gameObject, _damage);

        }
    }

    
}

