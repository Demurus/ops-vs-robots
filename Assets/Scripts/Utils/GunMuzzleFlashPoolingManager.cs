﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunMuzzleFlashPoolingManager : MonoBehaviour
{
    private static GunMuzzleFlashPoolingManager _instance;

   
    public GameObject GunMuzzleFlashPrefab;
    public int GunMuzzleFlashPrefabAmount = 4;
    private List<GameObject> _gunMuzzleFlashes;
    public Transform GunMuzzleFlashParent;
    public static GunMuzzleFlashPoolingManager Instance
    {
        get => _instance;
    }

    public GameObject GetGunMuzzleFlashPrefab()
    {
        foreach (GameObject gunMuzzleFlash in _gunMuzzleFlashes)
            if (!gunMuzzleFlash.activeInHierarchy)
            {
                gunMuzzleFlash.transform.position = GunMuzzleFlashParent.position;
                gunMuzzleFlash.SetActive(true);
                return gunMuzzleFlash;
            }
        GameObject prefabInstance = Instantiate(GunMuzzleFlashPrefab);
        prefabInstance.transform.SetParent(transform);
        prefabInstance.SetActive(false);
        _gunMuzzleFlashes.Add(prefabInstance);
        return prefabInstance;
    }

   
    private void Awake()
    {
        _instance = this;
      
        _gunMuzzleFlashes = new List<GameObject>(GunMuzzleFlashPrefabAmount);
        for (int i = 0; i < GunMuzzleFlashPrefabAmount; i++)
        {
            GameObject prefabInstance = Instantiate(GunMuzzleFlashPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            _gunMuzzleFlashes.Add(prefabInstance);
        }
    }
    void Start()
    {

    }
}
