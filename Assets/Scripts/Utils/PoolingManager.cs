﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolingManager : MonoBehaviour
{
    private static PoolingManager _instance;

    public GameObject SimpleBulletPrefab;
    public int SimpleBulletAmount = 25;
    private List<GameObject> _simplebullets;

    public static PoolingManager Instance
    {
        get => _instance;
    }


    public GameObject GetSimpleBullet()
    {
        foreach(GameObject bullet in _simplebullets)
        {
            if (!bullet.activeInHierarchy)
            {
                bullet.SetActive(true);
                bullet.GetComponentInChildren<SimpleBullet>().Fire();
                return bullet;
            }
        }
        GameObject prefabInstance = Instantiate(SimpleBulletPrefab);
        prefabInstance.transform.SetParent(transform);
        _simplebullets.Add(prefabInstance);
        return prefabInstance;
    }
    private void Awake()
    {
        _instance = this;
        _simplebullets = new List<GameObject>(SimpleBulletAmount);
        for(int i = 0; i < SimpleBulletAmount; i++)
        {
            GameObject prefabInstance = Instantiate(SimpleBulletPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            _simplebullets.Add(prefabInstance);
        }

       
    }
    void Start()
    {
        
    }

    
}
