﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleMuzzleFlashPoolingManager : MonoBehaviour
{
    private static RifleMuzzleFlashPoolingManager _instance;


    public GameObject RifleMuzzleFlashPrefab;
    public int RifleMuzzleFlashPrefabAmount = 30;
    private List<GameObject> _rifleMuzzleFlashes;
    public Transform RifleMuzzleFlashParent;
    public static RifleMuzzleFlashPoolingManager Instance
    {
        get => _instance;
    }

    public GameObject GetRifleMuzzleFlashPrefab()
    {
        foreach (GameObject rifleMuzzleFlash in _rifleMuzzleFlashes)
            if (!rifleMuzzleFlash.activeInHierarchy)
            {
                rifleMuzzleFlash.transform.position = RifleMuzzleFlashParent.position;
                rifleMuzzleFlash.SetActive(true);
                return rifleMuzzleFlash;
            }
        GameObject prefabInstance = Instantiate(RifleMuzzleFlashPrefab);
        prefabInstance.transform.SetParent(transform);
        prefabInstance.SetActive(false);
        _rifleMuzzleFlashes.Add(prefabInstance);
        return prefabInstance;
    }


    private void Awake()
    {
        _instance = this;

        _rifleMuzzleFlashes = new List<GameObject>(RifleMuzzleFlashPrefabAmount);
        for (int i = 0; i < RifleMuzzleFlashPrefabAmount; i++)
        {
            GameObject prefabInstance = Instantiate(RifleMuzzleFlashPrefab);
            prefabInstance.transform.SetParent(transform);
            prefabInstance.SetActive(false);
            _rifleMuzzleFlashes.Add(prefabInstance);
        }
    }
}
