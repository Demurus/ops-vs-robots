﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTags
{
    public const string ShootTrigger = "Shoot";
    public const string ReloadTrigger = "Reload";
    public const string AimState = "Aim";
    public const string WalkState = "Walk";
    public const string ZoomCameraIn = "ZoomIn";
    public const string ZoomCameraOut = "ZoomOut";
}

public class EnemyAnimationTags
{
    public const string ShootTrigger = "Shoot";
    public const string DeadTrigger = "Dead";
    public const string WalkBool = "Walk";
    public const string RunBool = "Run";
}

public class CommonTags
{
    public const string PlayerTag = "Player";
    public const string Enemytag = "Enemy";
}

public class EnvironmentTags
{
    public const string Concrete = "Concrete";
    public const string Wood = "Wood";
    public const string Metal = "Metal";
}

public class SendMessagesTags
{
    public const string ApplyDamage = "ApplyDamage";
    public const string ApplyDamageToPlayer = "ApplyDamageToPlayer";
}

public class SoundTags
{
    public const string Music = "Music";
    public const string CommonAmbience = "CommonAmbience";
    public const string AmbienceDetail = "AmbienceDetail";
}