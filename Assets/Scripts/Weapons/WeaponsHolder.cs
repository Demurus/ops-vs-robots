﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponsHolder: MonoBehaviour
{
    [SerializeField] private WeaponsModel[] _weaponModels;

    private byte _activeWeaponIndex;

    private WeaponsModel CarriedWeapon1;
    private WeaponsModel CarriedWeapon2;
    private WeaponsModel CurrentlySelectedweapon;

    private void Awake()
    {
        CarriedWeapon1 = _weaponModels[0];
        CarriedWeapon2 = _weaponModels[1];
    }
    void Start()
    {
        CurrentlySelectedweapon = CarriedWeapon1;
        CurrentlySelectedweapon.gameObject.SetActive(true);
    }
    private void WeaponShootChecker()
    {

        if (GetCurrentSelectedWeapon()._weaponFireType == WeaponFireType.Multiple)
        {
            if (Input.GetMouseButton(0))
            {
                GetCurrentSelectedWeapon().Shoot();
              
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                GetCurrentSelectedWeapon().Shoot();
            }
        }

    }

    private void ReloadWeapon()
    {
          GetCurrentSelectedWeapon().Reload();
    }
    private void SwitchWeapon ()
    {
        if (CarriedWeapon1.gameObject.activeSelf)
        {

            CarriedWeapon1.gameObject.SetActive(false);
            CurrentlySelectedweapon = CarriedWeapon2;
            CarriedWeapon2.gameObject.SetActive(true);

        }
        else
        {
            CarriedWeapon1.gameObject.SetActive(true);
            CurrentlySelectedweapon = CarriedWeapon1;
            CarriedWeapon2.gameObject.SetActive(false);
        }

       
    }

    // Update is called once per frame
    void Update()
    {
        WeaponShootChecker();
        if (Input.GetKeyDown(KeyCode.R)) ReloadWeapon();
        if (Input.GetKeyDown(KeyCode.Q)) SwitchWeapon();
        
    }
    public WeaponsModel GetCurrentSelectedWeapon()
    {
        return CurrentlySelectedweapon;
    }
  
}
