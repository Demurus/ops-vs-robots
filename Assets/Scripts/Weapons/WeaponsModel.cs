﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponName
{
    Pistol,
    M4A1
}
public enum WeaponAim
{
    Aim,
    NoAim
}
public enum WeaponFireType
{
    Single,
    Triple,
    Multiple
}

public enum WeaponBulletType
{
    SimpleBullet,
    ArmorPiercingBullet,
    Grenade
}
[Serializable]
public class WeaponsModel:MonoBehaviour
{
    [SerializeField] private Animator _animator;
   protected AudioSource _audioSource;

    public WeaponName WeaponName;
    public WeaponAim _weaponAim;
    public WeaponFireType _weaponFireType;
    public WeaponBulletType _weaponBulletType;
       
    protected bool canShoot = true;
    
    private void Awake()
    {
        
    }

    public void ShootAnimation()
    {
        _animator.SetTrigger(AnimationTags.ShootTrigger);
    }
    public void ReloadAnimation()
    {
        _animator.SetTrigger(AnimationTags.ReloadTrigger);
    }

    protected void PlayShotSound(AudioClip clip)
    {
       
        _audioSource.PlayOneShot(clip);
    }
    protected void PlayEmptyClipSound (AudioClip clip)
    {
        _audioSource.PlayOneShot(clip);
    }
    protected void PlayReloadSound(AudioClip clip)
    {
         _audioSource.clip = clip;
        _audioSource.PlayOneShot(_audioSource.clip);
    }
    public virtual void Shoot()
    {
        Debug.Log("not implemented");
    }

    public virtual void Reload()
    {
        Debug.Log("not implemented");
    }


    public IEnumerator ReloadTimer(float reloadTime)
    {
        yield return new WaitForSeconds(reloadTime);
        
      
        canShoot = true;
    }
    public IEnumerator TimeToNexFire(float timer)
    {
        yield return new WaitForSeconds(timer);
        canShoot = true;

    }
    public IEnumerator DisableGunMuzzleFlashTimer(GameObject muzzleFlash)
    {
        yield return new WaitForSeconds(1.1f);
        muzzleFlash.SetActive(false);
    }
    public IEnumerator DisableBulletTimer(GameObject bullet)
    {
        yield return new WaitForSeconds(1f);
        bullet.SetActive(false);
    }

  
}
