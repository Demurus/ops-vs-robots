﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsZoomer : MonoBehaviour
{
    
    private WeaponsHolder _weaponsHolder;
   
    private Vector3 _originalPosition;
    [SerializeField] private Vector3 _aimPosition;
    [SerializeField] private float _aimSpeed=3f;
    [SerializeField] private GameObject _crossHair;
    private bool _zoomed;
    private Camera _mainCamera;


    
    private void Awake()
    {
        _weaponsHolder = GetComponent<WeaponsHolder>();
        _mainCamera = Camera.main;
        

    }

   

    
    private void ZoomWeaponCamera()
    {
        if (_weaponsHolder.GetCurrentSelectedWeapon()._weaponAim == WeaponAim.Aim)
        {
            if (Input.GetMouseButton(1))
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, _aimPosition, Time.deltaTime * _aimSpeed);
               
                _crossHair.SetActive(false);
                _zoomed = true;
            }

            else
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, _originalPosition, Time.deltaTime * _aimSpeed);
                _crossHair.SetActive(true);
            }
           
        }
    }
    void Start()
    {
        _originalPosition = transform.localPosition;
        Debug.Log(_originalPosition);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        ZoomWeaponCamera();
       
       
    }
}
